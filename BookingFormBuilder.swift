import UIKit

import ReactiveSwift

private extension Product
{
    func buildBookingFromProductInteractor() -> BookingFormInteractor
    {
        switch self.tag {
        case .Day(_): return BookingFormClassInteractor()
        default: return BookingFormProductInteractor()
        }
    }
}

class BookingFormBuilder: NSObject
{
    private static let account = UserAccountManager()
    
    private static func build(for products: [Product], with module: (interactor: BookingFormInteractor, presenter: BookingFormPresenter, viewController: BookingFormViewController)) -> UIViewController
    {
        let interactor = module.interactor
        let presenter = module.presenter
        let viewController = module.viewController
        
        let reward = DataStore.defaultMemoryRealm.objects(ReferralReward.self).first
        presenter.products = products.reduce(into: [String: Product]()) { $0[$1.objectID] = $1 }
        presenter.appliedReferral.swap(reward)
        
        if let user = account.currentUser {
            presenter.update(with: user)
        }
        
        let router = BookingFormRouter()
        let worker = BookingFormWorker()
        
        interactor.output = presenter
        interactor.worker = worker
        
        viewController.output = interactor
        viewController.router = router
        router.viewController = viewController
        presenter.output = viewController
        worker.stripeAuthenticationContext = viewController
        
        let nc = BookingFormNavigationController(navigationBarClass: PassThroughNavigationBar.self, toolbarClass: .none)
        nc.setViewControllers([viewController], animated: false)
        return nc
    }
    
    class func buildWith(product: Product, from products: [Product], at location: Address? = .none) -> UIViewController
    {
        let presenter = BookingProductFormPresenter(with: product, and: location)
        let viewController = BookingProductFormViewController()
        
        return build(for: products, with: (interactor: product.buildBookingFromProductInteractor(), presenter: presenter, viewController: viewController))
    }
    
    class func buildWith(user: User, and targets: (location: MutableProperty<Address?>, partner: MutableProperty<User?>, time: MutableProperty<Date?>)) -> UIViewController
    {
        let presenter = BookingPartnerFormPresenter(with: user, and: Address.DefaultAddress)
        presenter.startDate = targets.time
        let interactor = BookingFormPartnerInfoInteractor(with: targets.partner)
        let viewController = BookingPartnerInfoFormViewController()
        
        return build(for: [], with: (interactor: interactor, presenter: presenter, viewController: viewController))
    }
    
    @objc class func buildWith(user: User, and products: [Product], at location: Address? = .none) -> UIViewController
    {
        return buildWith(user: user, and: products, at: location, for: .none)
    }
    
    @objc class func buildWith(user: User, and products: [Product], at location: Address? = .none, for workout: Product?) -> UIViewController
    {
        let presenter = BookingPartnerFormPresenter(with: user, and: Address.DefaultAddress)
        presenter.selectedWorkout.swap(workout)
        
        let viewController = BookingPartnerFormViewController()
        
        return build(for: products, with: (interactor: BookingFormPartnerInteractor(), presenter: presenter, viewController: viewController))
    }
    
    @objc class func buildWith(booking: Booking) -> UIViewController
    {
        let partner = booking.partner!
        let location = booking.location.flatMap { Set(partner.coveredAreas).contains($0.outwardCode) ? $0 : .none }
        
        let types = TRBAppConfigurationManager.sharedInstance()?.products.reduce([String: Product]()) { $0.merging([$1.objectID: $1]) { $1 } } ?? [:]
        let products = Array(partner.specialities)
        products.forEach {
            $0.price = types[$0.objectID]?.price
            $0.finalPrice = types[$0.objectID]?.finalPrice
        }
        
        return buildWith(user: partner, and: products, at: location, for: products.first { $0.objectID == booking.product!.objectID })
    }
    
    class func build(withSubscription plan: SubscriptionPlan) -> UIViewController
    {
        let interactor = BookingSubscriptionFormInteractor(withPlan: plan);
        let viewController = BookingFormViewController()
        let presenter = BookingFormPresenter();
        
        return build(for: [], with: (interactor: interactor, presenter: presenter, viewController: viewController))
    }
}
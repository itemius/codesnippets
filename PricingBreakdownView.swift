import UIKit

import ReactiveSwift

struct PricingBreakdownViewModel
{
    let promocode: Action<Void, Void, Never>?
    let statements: [SignalProducer<(text: String, value: String, highlighted: BookingFormPriceBreakdownStatementType), Never>]
    let total: SignalProducer<(text: String, original: String, final: String), Never>
}

class PricingBreakdownView: UIView
{
    private weak var content: UIStackView!
    private var disposals = [Disposable]()
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private func setup()
    {
        backgroundColor = StyleKit.kTRBColourWhite
        clipsToBounds = true
        layer.cornerRadius = ExploreDefaultSpacing
        translatesAutoresizingMaskIntoConstraints = false
        
        let stack = UIStackView()
        stack.alignment = .fill
        stack.axis = .vertical
        stack.distribution = .fillProportionally
        stack.spacing = ExploreDefaultSpacing
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(stack)
        
        NSLayoutConstraint.activate(
            [
                stack.leftAnchor.constraint(equalTo: leftAnchor, constant: 2 * ExploreDefaultSpacing),
                stack.topAnchor.constraint(equalTo: topAnchor, constant: 3 * ExploreDefaultSpacing),
                rightAnchor.constraint(equalTo: stack.rightAnchor, constant: 2 * ExploreDefaultSpacing),
                bottomAnchor.constraint(equalTo: stack.bottomAnchor, constant: 3 * ExploreDefaultSpacing)
            ]
        )
        
        content = stack
    }
    
    func clear()
    {
        content.arrangedSubviews.forEach { $0.removeFromSuperview() }
        while let disposer = disposals.popLast() { disposer.dispose() }
    }
    
    func fill(with model: PricingBreakdownViewModel)
    {
        model.statements.map(statementRow(for:)).forEach(content.addArrangedSubview)
        
        content.addArrangedSubview(Divider(fitToEdges: true))
        
        let total = totalRow(for: model.total)
        content.addArrangedSubview(total)
        
        if let action = model.promocode {
            content.addArrangedSubview(Divider(fitToEdges: true))
            let promocode = promocodeRow(withAction: action)
            content.addArrangedSubview(promocode)
        }
    }
    
    private func statementRow(for data: SignalProducer<(text: String, value: String, highlighted: BookingFormPriceBreakdownStatementType), Never>) -> UIView
    {
        let row = stackForRow()
        let textColor = data.map { $0.highlighted.statementRowTextColor }
        
        let text = BodyLabel()
        (text.reactive.textColor <~ textColor).map { disposals.append($0) }
        (text.reactive.wrappedText <~ data.map { $0.text }).map { disposals.append($0) }
        row.addArrangedSubview(text)
        
        let value = BodyLabel()
        (value.reactive.textColor <~ textColor).map { disposals.append($0) }
        (value.reactive.wrappedText <~ data.map { $0.value }).map { disposals.append($0) }
        row.addArrangedSubview(value)
        
        text.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        value.setContentHuggingPriority(.required, for: .horizontal)
        
        return row
    }
    
    private func totalRow(for data: SignalProducer<(text: String, original: String, final: String), Never>) -> UIView
    {
        let row = stackForRow()
        
        let text = HeadlineLabel()
        text.textAlignment = .left
        text.reactive.wrappedText <~ data.map { $0.text }
        row.addArrangedSubview(text)
        
        let original = BodyLabel()
        original.textColor = StyleKit.kTRBColourGreyInactive
        original.setContentHuggingPriority(.required, for: .horizontal)
        
        row.addArrangedSubview(original)
        
        let final = GiganticLabel()
        row.addArrangedSubview(final)
        
        text.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        final.setContentHuggingPriority(.required, for: .horizontal)
        
        disposals.append(contentsOf:
            [
                original.reactive.makeBindingTarget {
                    $0.wrappedText = $1
                    let new = original.attributedText!.mutableCopy() as! NSMutableAttributedString
                    new.strikethrough()
                    $0.attributedText = new
                } <~ data.map { $0.original },
                final.reactive.wrappedText <~ data.map { $0.final }
            ]
                .compactMap { $0 }
        )
        
        return row
    }
    
    private func promocodeRow(withAction action: Action<Void, Void, Never>) -> UIView
    {
        let button = UIButton(type: .custom)
        button.reactive.pressed = CocoaAction(action)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let row  = stackForRow()
        row.isUserInteractionEnabled = false
        row.spacing = 2 * ExploreDefaultSpacing
        
        let promo = OneLineTwoIconOneLabelListItem()
        promo.isUserInteractionEnabled = false
        
        promo.mainIcon.image = StyleKit.imageOfBonusIcon()
        promo.mainIcon.setContentHuggingPriority(.required, for: .horizontal)
        row.addArrangedSubview(promo.mainIcon)
        
        promo.mainText.textColor = StyleKit.kTRBColourRed
        promo.mainText.wrappedText = "Apply promo code"
        promo.mainText.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        row.addArrangedSubview(promo.mainText)
        
        promo.secondaryIcon.image = StyleKit.imageOfRightIcon(imageSize: CGSize(width: 3 * ExploreDefaultSpacing, height: 3 * ExploreDefaultSpacing), on: false)
        promo.secondaryIcon.setContentHuggingPriority(.required, for: .horizontal)
        row.addArrangedSubview(promo.secondaryIcon)
        
        button.addSubview(row)
        
        NSLayoutConstraint.activate(
            [
                row.leftAnchor.constraint(equalTo: button.leftAnchor),
                row.topAnchor.constraint(equalTo: button.topAnchor),
                button.rightAnchor.constraint(equalTo: row.rightAnchor),
                button.bottomAnchor.constraint(equalTo: row.bottomAnchor)
            ]
        )
        
        return button
    }
    
    private func stackForRow() -> UIStackView
    {
        let row = UIStackView()
        row.alignment = .fill
        row.axis = .horizontal
        row.distribution = .fill
        row.spacing = ExploreDefaultSpacing
        row.translatesAutoresizingMaskIntoConstraints = false
        return row
    }
}

private extension BookingFormPriceBreakdownStatementType
{
    var statementRowTextColor: UIColor {
        switch self {
        case .disabled: return StyleKit.kTRBColourGreyInactive
        case .highlitghted: return StyleKit.kTRBColourRed
        case .normal: return StyleKit.kTRBColourBlack
        }
    }
}
import UIKit
import CoreServices

import AlamofireImage
import ReactiveSwift

import Model

class ProductImageRequestCache: ImageRequestCache {
    private var Manager: FileManager { return FileManager.default }
    
    private lazy var Images: URL = {
        let url = Manager.urls(for: .cachesDirectory, in: .userDomainMask).first!.appendingPathComponent("Products", isDirectory: true)
        var isDirectory: ObjCBool = false
        if !Manager.fileExists(atPath: url.path, isDirectory: &isDirectory) {
            do { try Manager.createDirectory(at: url, withIntermediateDirectories: false, attributes: .none) } catch {
                print("\n\n Unable to create Products folder \n\n")
            }
        }
        return url
    } ()
    
    private func fileURL(for identifier: String) -> URL
    {
        return Images.appendingPathComponent("\(identifier.hash)" + (URL(string: identifier).map { ".\($0.pathExtension)" } ?? ""))
    }
    
    func add(_ image: Image, for request: URLRequest, withIdentifier identifier: String?) {
        (identifier ?? request.url?.absoluteString).map { add(image, withIdentifier: $0) }
    }
    
    func removeImage(for request: URLRequest, withIdentifier identifier: String?) -> Bool {
        (identifier ?? request.url?.absoluteString).map(removeImage(withIdentifier:)) ?? false
    }
    
    func image(for request: URLRequest, withIdentifier identifier: String?) -> Image? {
        return (identifier ?? request.url?.absoluteString).flatMap(image(withIdentifier:))
    }
    
    func add(_ image: Image, withIdentifier identifier: String) {
        if let file = image.pngData() ?? image.jpegData(compressionQuality: 1.0) {
            do { try file.write(to: fileURL(for: identifier), options: .atomicWrite) }
            catch {
                print("\n\n Image with Identifier \(identifier) could not be written to \(fileURL(for: identifier)) \n\n")
            }
        }
    }
    
    func removeImage(withIdentifier identifier: String) -> Bool {
        var removed = true
        do { try Manager.removeItem(at: fileURL(for: identifier)) }
        catch {
            print("\n Image with Identifier \(identifier) could not be removed from \(fileURL(for: identifier)) \n" + error.localizedDescription + "\n\n")
            removed = false
        }
    
        return removed
    }
    
    func removeAllImages() -> Bool {
        let keys = Set<URLResourceKey>([.isRegularFileKey])
        guard let enumerator = Manager.enumerator(at: Images, includingPropertiesForKeys: Array(keys)) else { return false }
        
        var result = true
        for case let url as URL in enumerator {
            if let properties = try? url.resourceValues(forKeys: keys), properties.isRegularFile ?? false {
                do { try Manager.removeItem(at: url) }
                catch {
                    result = false
                }
            }
            else {
                result = false
            }
        }
        return result
    }
    
    func image(withIdentifier identifier: String) -> Image? {
        return (try? Data(contentsOf: fileURL(for: identifier))).flatMap { UIImage(data: $0, scale: UIScreen.main.nativeScale) }
    }
}

class TRBProductConfigurator: NSObject
{
    static let Downloader = ImageDownloader(imageCache: ProductImageRequestCache())
    
    private var downloader: ImageDownloader { return TRBProductConfigurator.Downloader }
    private let profile = UserProfileRequest()
    
    let product: Product
    
    @objc init(withProduct object: Product)
    {
        product = object
        super.init()
    }
    
    fileprivate func loadImage(fromUrl url: URL, usingGroup group: DispatchGroup, onCompletion handler:@escaping (UIImage) -> ()) {
        group.enter()
        downloader.download(URLRequest(url: url), completion: {
            if case .success(let image) = $0.result {
                handler(image)
            }
            group.leave()
        })
    }
    
    fileprivate func URLFromPath(_ path: String) -> URL? {
        let path = URL(string: path)
        if let ext = path?.pathExtension {
            return path.map { $0.deletingPathExtension().appendingPathComponent("\(Int(UIScreen.main.nativeScale))x", isDirectory: false).appendingPathExtension(ext) }
        }
        else {
            return path
        }
    }
    
    @objc func configure(usingGroup group: DispatchGroup)
    {
        let object = product
        
        object.coverImagePath.flatMap(URLFromPath).flatMap { loadImage(fromUrl: $0, usingGroup: group) { object.coverImage = $0 } }
        object.featuresDistributionImagePath.flatMap(URLFromPath).flatMap { loadImage(fromUrl: $0, usingGroup: group) { object.featuresDistributionImage = $0 } }
        
        if let partnerID = object.partner?.objectID {
            var token: Disposable?
            object.partner?.reviews.removeAll()
            token = profile.paginatedReviewsAbout(trainer: partnerID, loadNext: .empty).on {
                if let reviews = $0.contentList {
                    object.partner?.reviews.append(objectsIn: reviews)
                }
                token?.dispose()
            }.start()
        }
    }
}
import Foundation

import ReactiveSwift
import ReactiveObjCBridge

class UserAppFlowManager: AppFlowManager
{
    private let account = UserAccountManager()
    private let requests = UserProfileRequest()
    private let payment = PaymentRequest()
    private let subscriptions = SubscriptionWorker()
    
    private var verifyReferral: ((PaymentRequestTasks) -> SignalProducer<ReferralReward, NSError>?) = { _ in .none }
    
    override func setupAfterLogin()
    {
        let continuation = super.setupAfterLogin
        
        var setup = TRBAppConfigurationManager.sharedInstance()!.getSessionTypes()!
        setup = setup.merge((subscriptions.loadPlans().map { $0 as AnyObject } as SignalProducer<AnyObject, NSError>).bridged)
        
        if let user = account.currentUser
        {
            let cardManager = TRBCardManager.sharedInstance()
            cardManager.methodListNeedsUpdate = true;
            cardManager.updatePaymentMethods()
            
            let realm = DataStore.userRealm
            let main = Thread.isMainThread ? { $0.perform() } : DispatchQueue.main.sync
            let manager = account
            main(
                DispatchWorkItem {
                    if let home = realm.object(ofType: Address.self, forPrimaryKey: "Home") {
                        manager.currentUser!.update { user in
                            user.realm!.object(ofType: Address.self, forPrimaryKey: "Home").map { user.realm!.delete($0) }
                            user.homeLocation = Address(value: home)
                        }
                        manager.updateHomeLocation(home).start()
                        try? realm.write { realm.delete(home) }
                    }
                    
                    if let work = realm.object(ofType: Address.self, forPrimaryKey: "Work") {
                        manager.currentUser!.update { user in
                            user.realm!.object(ofType: Address.self, forPrimaryKey: "Work").map { user.realm!.delete($0) }
                            user.workLocation = Address(value: work)
                        }
                        manager.updateWorkLocation(work).start()
                        try? realm.write { realm.delete(work) }
                    }
                }
            )
            
            TruBeAnalytics.yam_setProfile(with: user)
        }

        setup.deliverOnMainThread().delay(0.6).subscribeCompleted { continuation() }
    }
    
    override func handleReferral(_ code: String, from name: String)
    {
        let realm = DataStore.defaultMemoryRealm
        let verification = { (tasks: PaymentRequestTasks) in
            tasks
                .verifyReferralCode(code)
                .map { $0.contentObject! }
                .on() { reward in
                    reward.from = name
                    realm.beginWrite()
                    
                    realm.add(reward, update: .modified)
                    try! realm.commitWrite()
                }
                .flatMapError { _ in SignalProducer<ReferralReward, NSError>.empty }
        }
        
        if requests.accountManager.currentUser != nil
        {
            verification(payment).start()
        }
        else
        {
            verifyReferral = verification
        }
    }
}